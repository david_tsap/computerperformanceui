import { Component, OnInit } from '@angular/core';
import { Computer } from '../module/computer';
import { MaxPerformance } from '../module/MaxPerformance';
import { WebSocketService } from '../service/web-socket.service';

@Component({
  selector: 'app-cpu-tester',
  templateUrl: './cpu-tester.component.html',
  styleUrls: ['./cpu-tester.component.css']
})
export class CpuTesterComponent implements OnInit {

  ramValue = 0;
  cpuValue = 0;
  title = 'computer-client';
  computer: Computer ;
  maxComp : MaxPerformance = new MaxPerformance();

  constructor(private webSocket: WebSocketService){
  }
  ngOnInit(): void {
    this.webSocket.computerObservable.subscribe(computer =>{ 
      this.computer = computer;
      console.log(this.computer)
    });
  }

  onInputChange(){
    this.maxComp.maxCpu = this.cpuValue.toString();
    this.maxComp.maxRam = this.ramValue.toString();
    this.webSocket.sendMessage(this.maxComp);
  }
}
