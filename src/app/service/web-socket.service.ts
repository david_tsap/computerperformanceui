import { Injectable } from '@angular/core';
import { Computer } from '../module/computer';
import { Stomp } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { MaxPerformance } from '../module/MaxPerformance';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  private websocketUrl: string = "http://localhost:8080/pref";
  private websocketSendMessageUrl: string = "/api/setMax";
  public computer:BehaviorSubject<Computer> = new BehaviorSubject(new Computer);
  computerObservable = this.computer.asObservable()
  private stompClient = null;


  constructor() { 
    const socket = new SockJS(this.websocketUrl);
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log('Connected: ' + frame);
      _this.stompClient.subscribe('/topic/performance', function(comp) {
        _this.updateParams(JSON.parse(comp.body));
      });
    });
  }
  updateParams(comp: Computer) {
    console.log("get comp from server" + comp);
    this.computer.next(comp);
  }

  sendMessage(maxPerformance:MaxPerformance):void{
    console.log("calling setMax api via web socket");
    this.stompClient.send(this.websocketSendMessageUrl, {}, JSON.stringify(maxPerformance));
  }

}
